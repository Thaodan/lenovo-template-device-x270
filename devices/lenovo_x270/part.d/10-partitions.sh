#!/bin/sh

parted --script /dev/nvme0n1  \
       mklabel gpt \
       mkpart primary fat32 '0%' 200MB\
       name 1 EFI-System \
       set 1 esp on \
       set 1 boot on

parted --script /dev/nvme0n1 \
       mkpart primary ext4 201MB 100%\
       name 2 Lvm \
       set 2 lvm on
sync

mkfs.vfat -F32 /dev/nvme0n1p1
