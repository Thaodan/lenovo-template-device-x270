#!/bin/sh

dd bs=512 count=4 if=/dev/urandom of=$PWD/keyfile

cryptsetup -q luksFormat /dev/nvme0n1p2 $PWD/keyfile

cryptsetup open /dev/nvme0n1p2 --key-file $PWD/keyfile LVM
