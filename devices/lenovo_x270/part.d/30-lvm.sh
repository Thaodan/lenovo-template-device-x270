#!/bin/sh

pvcreate -ff /dev/mapper/LVM


vgcreate VolGroup00 /dev/mapper/LVM

# create system partition
lvcreate -L 50G VolGroup00 -n System
# create home partition
lvcreate -L 400G VolGroup00 -n Home
lvcreate -L 8G VolGroup00 -n Swap

mkfs.ext4 /dev/mapper/VolGroup00-System 
mkfs.ext4 /dev/mapper/VolGroup00-Home
mkswap /dev/mapper/VolGroup00-Swap
